---
Subtitle:			Resume
Author:				Yisrael Dov Lebow
Address:			Dov Hoz 5
					Ashdod
					Israel
Phone:				+972548493912
Email:				lebow@lebowtech.com
Date:				6th of Nisan, 5783
---

# Summary

Experienced technical professional with over 20 years of expertise in
software development, project management, and Agile
methodologies. Dedicated to crafting high-quality software that is
maintainable, extensible, and performs well. Proficient in multiple
programming languages, frameworks, and tools, with a passion for
staying up-to-date with the latest trends and best practices. Skilled
at leading teams and mentoring team members to help them improve their
skills and achieve their full potential.

# Skills #

* Project Management: Experienced in using both Kanban and Scrum
  methodologies to manage software development projects and teams.
* Programming/Scripting Languages: Highly proficient in
  Clojure/ClojureScript, LISP , PHP, Python, JavaScript/TypeScript, NodeJS,
  SQL (MySQL, MS SQL, SQLite), Shell (Bash), and Emacs-LISP.
* Frameworks: Skilled in using a variety of frameworks such as Reagent
  (ReactJS), AngularJS, Bootstrap, VueJS, Laravel, Slim, CakePHP,
  Zend, and Flask to develop web applications.
* Test Driven Development: Strong experience in implementing Test
  Driven Development (TDD) using **Cypress.io** and other testing
  frameworks to ensure high-quality software development.  Version
  Control: Proficient in using Git, Gitlab Continuous Integration, and
  Subversion (SVN) for version control and team collaboration.
* Web Technologies: Experienced in implementing REST, AJAX, SOAP,
  XMLRPC, XML, and gettext for web applications development.
* Containerization: Skilled in using Docker for containerization and
  deployment of software applications.
* Operating Systems: Proficient in working with Unix, Gnu/Linux,
  Mac0S, and NixOS.
* Languages: Bilingual in **English** and **Hebrew**, with mother tongue
  fluency in English and high-level proficiency in Hebrew.

# Experience #

## Feb 2005 - Present (Intermittently): Software Engineering Consultant at [Lebow Technology Solutions][lts] ##

- Coached organizations on software development practices.
- Managed remote teams and sourced recruited talent as needed.
- Maintained websites and managed servers for clients.
- Engineered and implemented web-based applications for various clients.
- Skills and Technologies: Clojure/ClojureScript (LISP), Reagent (ReactJS),
  Nix, AngularJS, CakePHP, Laravel, MySQL, Python, Emacs-LISP




## Jul 2021 - Feb 2022: R&D Manager at Practitest in Rehovot, Israel ##

- Managed a team of six developers in their day-to-day tasks.
- Optimized the development process and migrated the team to a more structured Kanban/Agile process.
- Mentored and coached the team in their activities to improve their technical skills.
- Researched and solved technical problems to ensure project success.
- Utilized tools such as ClojureScript (LISP), Ruby, Rails, RSpec, Nix, Kanban, Git, CircleCI, and GitHub Actions.


## Jun 2019 - Jun 2021: CTO/Technical Lead at Max45 (Remote) ##

- Lead technical development of a startup project involving hardware and software solutions.
- Collaborated with external consultants to design and implement custom PCBs.
- Utilized ClojureScript and Python to develop software components for the project.
- Managed the project timeline and priorities, ensuring timely delivery of milestones.
- Adapted to shifting priorities and changes due to the COVID-19 pandemic.
- Tools: ClojureScript (LISP), Python, Raspberry Pi, KiCad (LISP), GitLab-CI, Kanban.


## Nov 2017 - Jun 2019: Senior Software Developer at Gisha Internet Solutions in Holon ##

- Rebuilt a mission-critical application and advised on the appropriate tools to use for the job.
- Designed and implemented a CI/CD, including introducing DevOps practices.
- Worked as part of a small team for a startup project.
- Collaborated with cross-functional teams to ensure successful project delivery.
- Tools: Docker, Gitlab-CI, Laravel, PHP-Unit, PHP-Spec, Vue.js, CentOS, Ubuntu, Angularjs, AWS



## April 2008 - Oct 2013: Senior Software Developer at Wingate Ventures in Tel Aviv##

*   Engineered and implemented multi-lingual *white label* sites.
*   Maintained and upgraded portal sites.
*   Developed a custom call center software.
*   Developed systems for affiliate management and advertising.
*   Responsible for the evaluating the technical skills of potential new employes.
* Tools: AJAX, XML, CakePHP, Zend Framework, MySql, Joolma, Word Press, Apache,
    PHP, Python, JavaScript, Get text, Git, SVN


## Nov 2004 - May 2006: Software Developer at DNA Studio in Beverly Hills CA. ##

*   Engineered and implemented back-end to web based applications
*   Applications used by companies such as Sony, Casio, Budweiser and Cingular
*   Tools:AJAX, XML, SOAP, PHP,ASP, SQL (MySql and MSSQL), HTML, CSS, JavaScript

## Feb 2005 - Feb 2006: Software Developer at R/Com Networks in Corona Del Mar CA. ##

*   Initiated and carried out transition to CVS. 
*   Engineered and implemented back-end to web based applications
*   Tools: PHP, CVS, SQL (MySql), HTML, CSS, JavaScript


##  2004: Web Administrator / Developer at One Less Car in Annapolis MD.


*   Designed custom interface to Internet standard for calendar management
*   Assured that web content was current and relevant
*   Tools: PHP, HTML, CSS.

## 1999-2001: Network Technician at Mac Specialist at University of Maryland, College Park. ##

*   Maintained computer labs used by students.
*   Provided on-call technical support to staff and students for computer related problems.
*   Repaired computers belonging to faculty and staff.
*   Set up new personal computers for faculty, customized to work effectively with the campus network and to run required applications.

## 2000-2001: Software Developer at University of Maryland, College Park. ##

*   Designed web based front end for SQL database.
*   Updated old user interfaces to conform with a revised style
*   Analyzed efficacy of old programs and improved ones that were either poorly written or incomplete.
*   Tools: ASP, HTML, and Java Script.

## 2001: Web Developer at Center For Substance Abuse Research (CESAR). College Park MD. ##

*   Maintained government web site providing information and statistics about substance abuse in Maryland.
*   Streamlined existing HTML coding.

# Education #

## Bachelor of Science in Computer Engineering. ##
### University of Maryland at College Park. 2003

My Computer Engineering degree from the University of Maryland
equipped me with a strong foundation in both electrical engineering
and computer science. Through this program, I gained extensive
experience in both high-level and low-level programming languages,
with a focus on object-oriented programming. I also have a deep
understanding of computer architecture and its fundamental components,
as well as how they are implemented in commonly used systems.

My education also included studies in digital and analog circuit
design, with a particular focus on computer applications. This further
solidified my electrical engineering background and enabled me to
develop software solutions that are both efficient and effective.

Overall, my education and technical skills have equipped me with the
necessary tools to tackle real-world applications at the intersection
of electrical engineering and computer science.

# Other Work Experience #

## 2004 Office Manager/ Technical Support. Annapolis Osprey. Annapolis, MD ##

*   **Management of employees** --- Managed scheduling and payroll for employees. Developed protocol to improve employee efficiency. Monitored activity of employees to prevent losses . Diffused disciplinary situations 
*   **Accounting** --- 
      Reviewed the Daily sales summary for any potential problems. Tracked purchases and returns, daily and monthly.
      Prepared billing invoices for large accounts.
      Implemented new streamlined daily accounting procedures.
      Over-saw ordering based on evaluation of product profit margin analysis 
      Used *QuickBooks* Accounting software.
*   **Technical Support** --- 
      Repaired or solved any or network related questions to keep all computers up-to-date and running smoothly. 
      Designed, implemented, and maintained a new small office network.


[lts]: https://lebowtech.com/
