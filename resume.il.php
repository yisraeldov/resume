<?php
if (@$_COOKIE['sawResume']!=1) {
    mail('resume@lebowtech.com', 'Some One Viewed your resume', print_r($_SERVER, 1));
    setcookie('sawResume', 1);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="noarchive"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="resume.css" type="text/css"  charset="utf-8">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">


        <title>
            Yisrael Dov Lebow - Resume
        </title>
    </head>
    <body>
        <?PHP require_once("../googletracking.php");?>
        <div class='noprint' id="nav" onmouseout="hideNav();" onmouseover="showNav();"></div>

        <div class="bsd">
            בס''ד
        </div>


        <h2 style="display:none">
            Contact Info
        </h2>
        <!--img src="http://chart.apis.google.com/chart?cht=qr&chs=75&choe=UTF-8&chl=http://lebowtech.com/resume" class="noscreen" id="qrcode" / -->
        <?php require_once('contactInfo.html'); ?>
         <!--Include the markdown code -->
        <?php echo `./mmd4/multimarkdown  -a -s   ./resume.txt ` ;?>
        <div class="noprint">
            <a name='portfolio' ></a>
            <h2 id="portfolio">
                Portfolio
            </h2>
            <h3>
                Nov 2007 <a href="http://monthlytravel.com" target='_blank'>Monthly Travel</a>
            </h3><img src="MonthlyTravel.png" alt="Monthly Travel Screen Shot" width="80" height="60" onmouseover="imgEnlarge(this);" onmouseout="imgShrink(this);" />
            <h3>
                Jun 2007 <a href="http://torahforme.org" target='_blank'>TorahForMe</a>
            </h3><iframe src="http://torahforme.org/" width="80" height="60"   frameborder="0" scrolling='no'>Sorry, Sample not available.</iframe>
            <ul>
                <li>Tools:PHP,RSS (PodCasts),JavaScript
                </li>
                <li>Description:An automatically generated listing of lectures in MP3 format. the listing can be viewed in a web-browser as a collapsable directory tree, or as a PodCast / RSS feed. When viewing the listing in a web-browser the lectures can be heard on-line, or downloaded. The goal of this site was to allow the owner to add content as quickly as possible, with out the need to fill-in web forms or upload files one at a time.
                </li>
                <li>Client Feedback:
                    <blockquote>
                        Dear Mr. Lebow,<br />
                        <br />
                        I wanted to take this opportunity to thank you for your untiring and dedicated work on behalf of our website <a href="http://torahforme.org" target="_blank">www.torahforme.org</a>. Your work has been exceptional! You gave us unparalleled advice and matched our budget needs. You had the foresight and experience to correctly guide us in deciding what kind of website we needed. You built us a product that matched our needs exactly, something simple and quick; easy to work with and effortless to learn to maintain. Your proficiency and experience have been a tremendous asset to our organization. Your ongoing technical support exceeds our expectations! It has been an honor and pleasure to work with you!<br />
                        <br />
                        Thank you very much!<br />
                        Netanel Frankenthal<br />
                        Lecturer and Owner of <a href="http://torahforme.org" target="_blank">www.torahforme.org</a>
                    </blockquote>
                </li>
            </ul>
            <h3>
                Jun 2007 <a href="http://jumpsnap.com" target="_blank">Jump Snap</a>
            </h3><img src="jumpsnap.png" alt="JumpSnap Screen Shot" width="80" height="60" onmouseover="imgEnlarge(this);" onmouseout="imgShrink(this);" />
            <ul>
                <li>Tools:PHP, AJAX, MySQL, JavaScript, Subversion, ZenCart/OS-Commerce
                </li>
                <li>Description: An extensive modification of ZenCart eCommerce software. Some of the modifications include:
                    <ul>
                        <li>Converting the original multi-page check-out procedure into a single AJAX page.
                        </li>
                        <li>Creating a shopping cart, that the user can modify while shopping.
                        </li>
                        <li>Creating a custom feed of order info to the fulfillment-house.
                        </li>
                        <li>Create new <q>order total modules</q> for calculating fees specific to the clients needs.
                        </li>
                    </ul>This site is intended to be the first in a project to create a easily customizable online store, that can be cloned with a few clicks, all linked into a 'dashboard' that would allow the site admin to see the status of all his stores at once.
                </li>
            </ul>
            <h3>
                Dec 2006 <a href="http://repny.com" target="_blank">Repny.com</a>
            </h3><img src="repny.png" alt="Repny.com Shot" width="80" height="60" onmouseover="imgEnlarge(this);" onmouseout="imgShrink(this);" />
            <ul>
                <li>Tools:PHP, HTML, JavaScript, Subversion.
                </li>
                <li>Description:When I came on this project the site was in serious need of repair, most of my work was repairing the site and moving the site from a windows server to a Unix server. The project came to an early end because of lack of funding.
                </li>
            </ul>
            <h3>
                Apr 2006 <a href="http://www.wireless.att.com/source/" target="_blank">Cingular Source</a>
            </h3>
            <div class='linkNotes'>
                Due to Cingular's merger with AT&amp;T this site is no longer available in its original format.
            </div>
            <ul>
                <li>Tools:ASP, PHP, MySql, MsSql, CSS, JavaScript, AJAX
                </li>
                <li>Description:This site was a merging of many Cingular sites into one. I was responsible for almost all the implementation of the new site. In addition to moving data from the old servers to a new one, I created a new site navigation and new content management systems. Some of these content management systems used AJAX. While testing the migration, I also repaired many bugs with in the previous software.
                </li>
            </ul>
            <h3>
                Apr 2006 <a href="http://exilim.casio.com/" target="_blank">Casio Exilim - Choose A Camera</a>
            </h3><img src="casio_choose.png" alt="Casio Choose a Camera Screen Shot" width="80" height="60" onmouseover="imgEnlarge(this);" onmouseout="imgShrink(this);" />
            <div class='linkNotes'>
                Click on <q>Choose The Right Camera.</q>
            </div>
            <ul>
                <li>Tools: PHP, XML
                </li>
                <li>Description: Users fill out a questionnaire, which then suggests 3 products based on the user entries. A PHP scoring method is used to determine the 3 top products that match. The information is then returned as an XML file to the flash where it will be displayed to the user.
                </li>
            </ul>
            <h3>
                Apr 2006 <a href="http://exilim.casio.com/" target="_blank">Casio Exilim - Store Locator</a>
            </h3><img src="casio_find_store.png" alt="Casio Find a Store Screen Shot" width="80" height="60" onmouseover="imgEnlarge(this);" onmouseout="imgShrink(this);" />
            <div class='linkNotes'>
                Click on <q>Find A Store</q>.
            </div>
            <ul>
                <li>Tools: PHP, XML, SOAP, MsSQL, GIS(<a href="http://www.esri.com/software/arcwebservices/index.html" target="_blank">ArcWeb</a>)
                </li>
                <li>Description: Users can search for stores based on proximity to their location, entered as a zip code or City and state. This was accomplished with 3 parts: a flash front end, a php back-end, and a <strong>G</strong>eographic <strong>I</strong>nformation <strong>S</strong>ystem web-service( arcweb ). The <strong>S</strong>imple <strong>O</strong>bject <strong>A</strong>ccess <strong>P</strong>rotocol was used to retrieve the coordinates from the GIS web-service and then using those coordinates, retrieved a SOAP object describing all of the locations that matched the users specifications. After the locations are retrieved and parsed, each store location is cross referenced with a SQL database to determine what products that store carries. The PHP takes all of this information and generates an XML file to send to the flash movie that will display the information to the user.
                </li>
            </ul>
            <h3>
                Feb 2006 Budweiser -Bud Bowl - Super Bowl Commercials
            </h3>
            <ul>
                <li>Tools: HTML, CSS, JavaScript
                </li>
                <li>Description: During the Super Bowl Budweiser commercials were shown, and immediately made available for download. Several versions of the page needed to be created so that they could be launched at the correct time during the Super Bowl.
                </li>
            </ul>
            <h3>
                Jun 2006 <a href="http://www.vehiclevoice.com/user_home.php" target="_blank">Vehicle Voice</a>
            </h3><img src="vehicelvoice.jpg" alt="Vehicle Voice Screen Shot" width="80" height="60" onmouseover="imgEnlarge(this);" onmouseout="imgShrink(this);" />
            <ul>
                <li>Tools: PHP, MySql, HTML, CSS, CVS
                </li>
                <li>Description:My major contributions were the user management system though I was also involved in many other facets of this large project. Originally the web-site would record survey data and forget the user. Now the system allows the user to log in and link together the data collected from any survey that he has taken. I also generated a user home page that lists each individual user's current status on surveys he can take, or has already taken. In addition, I also did many upgrades to the internal administration site.
                </li>
            </ul>
            <h3>
                Jan 2005 <a href="http://rucingular.com/sundance/gallery.asp" target="_blank">rucingular - Sundance Short Film Festival</a>
            </h3>
            <ul>
                <li>Tools: ASP, JavaScript, MS SQL, Quicktime, HTML, CSS
                </li>
                <li>Description: This site comprises a gallery where users can browse, view, and rate videos submitted by other users. The top rated movies are displayed on the main page weekly, based on the code from my earlier 'Short Film Festival' code.
                </li>
            </ul>
            <h3>
                Dec 2004 Cingular Sony V500 instant win / shopping spree sweepstakes
            </h3>
            <ul>
                <li>Tools: ASP, JavaScript, MS SQL, HTML, CSS
                </li>
                <li>Description: Contest entries are processed, verified, and stored contest through the site. Instant winners are required to watch a flash movie, then they instantly win a prize. The application handles all of the operations of the sweepstakes and e-mails a dynamically created spreadsheet to the clearing house once a week.
                </li>
            </ul>
            <h3>
                Dec 2004 <a href="http://www.sonypictures.com/movies/hitch/site/" target="_blank">Sony Pictures - Hitch. message board</a>
            </h3><img src="hitch_forum.png" alt="Hitch Message Board Screen Shot" width="80" height="60" onmouseover="imgEnlarge(this);" onmouseout="imgShrink(this);" />
            <ul>
                <li>Tools: PHP, JavaScript, MySql, HTML, CSS
                </li>
                <li>Description: Administration tool for moderating a flash message board. Back-end enabling flash movies to post and retrieve message board data to a database
                </li>
            </ul>
            <h3>
                Dec 2004 <a href="http://www.sonypictures.com/movies/hitch/site/" target="_blank">Sony Pictures - Hitch. Polls</a>
            </h3><img src="hitch_poll.png" alt="Hitch Poll Screen Shot" width="80" height="60" onmouseover="imgEnlarge(this);" onmouseout="imgShrink(this);" />
            <ul>
                <li>Tools: PHP, JavaScript, MySql, HTML, CSS
                </li>
                <li>Description: Back-end enabling flash movies to post and retrieve poll data to a database
                </li>
            </ul>
            <h3>
                Dec 2004 Cingular New York Regional Homepage.
            </h3>
            <ul>
                <li>Tools: PHP, JavaScript, MySql, HTML, CSS
                </li>
                <li>Description: The main page for Cingular New York city area. The content is all retrieved from a database. There is also an administrator page where the user can update, add, or remove promotions. (this site is not yet live)
                </li>
            </ul>
            <h3>
                Nov 2004 <a href="http://rucingular.com/screeningroom/screening_rooms.asp" target="_blank">rucingular - Short Film Festival</a>
            </h3>
            <p>
                Local-Mirror <a href="rucingular.com/screeningroom/screening_rooms.asp.html" target="_blank">rucingular - Short Film Festival</a>
            </p>
            <ul>
                <li>Tools: ASP, JavaScript, MS SQL, Quicktime, HTML, CSS
                </li>
                <li>Description: The website is a gallery where users can browse, view, and rate videos submitted by other users. The top rated movies are displayed on the main page weekly
                </li>
            </ul>
            <h3>
                2003 <a href="http://homepage.mac.com/jago_lebow/Php_tutorial/" target="_blank">PHP on OSX Tutorial</a>
            </h3>
            <ul>
                <li>Tools: HTML, CSS, (PHP)
                </li>
                <li>Description: A short tutorial
                </li>
            </ul>
            <p>
                The screen shots are included to give an idea of the sites functionality, not to demonstrate my ability as a designer. I take no credit or responsibility for the design on most of these sites
            </p>
        </div>
        <h2 id="references">
            References
        </h2>
        <p>
            Available upon Request.
        </p>
        <div class="noprint">
            <hr />
            <p>
                <a href="http://validator.w3.org/check?uri=referer"><img  style="border:0;width:88px;height:31px" src="http://www.w3.org/Icons/valid-xhtml10-blue" alt="Valid XHTML 1.0 Transitional" ></a>
                <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss-blue" alt="Valid CSS!"></a>
            </p>
        </div>
        <p>
            <a name="end" id="end"></a>
        </p>

    </body>
</html>
