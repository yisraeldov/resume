{ pkgs ? import <nixpkgs> { } }:
let
  selfUrl = "https://yisraeldov.gitlab.io/resume/";
in pkgs.stdenv.mkDerivation {
  nativeBuildInputs = with pkgs; [
    nodePackages.prettier
    pandoc
    python3Packages.weasyprint
    qrencode
    hebcal
  ];
  name = "ydl-resume";
  src = ./.;
  buildPhase = ''
    qrencode -t png -o site-qr.png ${selfUrl}
    pandoc -t pdf -f markdown -o resume.pdf -d defaults.yaml README.md
    pandoc -t html -f markdown -o index.html -d defaults.yaml README.md
    pandoc -t docx -f markdown -o resume.docx -d defaults.yaml README.md
  '';

  installPhase = ''
    mkdir -p $out
    cp -r *.pdf *.docx *.css *.svg *.png *.ico index.html fontawesome $out/
    cp README.md $out/resume.txt
  '';
}
